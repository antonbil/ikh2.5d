﻿using System;
using TMPro;
using UnityEngine;

/// <summary>
/// class to force an info-object to always face the camera
/// </summary>
public class AlwaysFacingCameraInfoObject : MonoBehaviour
{
    
    [NonSerialized]
    public Vector3 middle;

    private GameObject _camera;

    void Update()
      {
          //transform.LookAt(Camera.main.transform.position, Vector3.up);
          //Transform transform1;
          //(transform1 = transform).Rotate(0, 180, 0);
          //Vector3 h = _camera.transform.position;

          //transform1.position = middle + (h - middle) / 5;
          //transform.position = middle + (h - middle) / 2;
          //print("position info"+(middle + (h - middle) / 5));
      }

      void Awake()
      {
          // this._camera = GameObject.Find("Main Camera");
          // print("camera:"+(_camera.transform.position));
          // Ray ray = new Ray(cp, middle-cp);
          // RaycastHit hitData;
          // Physics.Raycast(ray, out hitData);
          //transform.LookAt(Camera.main.transform.position, Vector3.up);

          //print("hit:"+(hitData.point));
          //transform.position = hitData.point;
          //transform.LookAt(Camera.main.transform.position, Vector3.up);
          //Transform transform1;
          //(transform1 = transform).Rotate(0, 180, 0);
          //Vector3 h = _camera.transform.position;
          //transform.LookAt(Camera.main.transform.position, Vector3.up);

      }

      public void SetText(string text)
      {
          GetComponent<TextMeshPro>().text = text;
      }

      public void SetOrigin(string name)
      {
           GameObject origin = GameObject.Find(name);
          Transform originTransform = origin.transform;
          //do not display line for demo-purposes
          /*
          GameObject newLine = new GameObject("Line");
          newLine.transform.SetParent(this.transform);
          LineRenderer lRend = newLine.AddComponent<LineRenderer>();
          // set the color of the line
          lRend.startColor = Color.red;
          lRend.endColor = Color.red;
 
          // set width of the renderer
          lRend.startWidth = 0.3f;
          lRend.endWidth = 0.3f;
 
          // set the position
          lRend.SetPosition(0, originTransform.position);
          lRend.SetPosition(1, transform.position);
          */

      }

      public void SetDistance(string name, float distance)
      {
          this.middle = GameObject.Find("Table_OfficeDesk_center").transform.position;
          Vector3 cp1 = Camera.main.transform.position;
          transform.position = (middle + (cp1 - middle) / 3) + new Vector3(0,1,0);
          GameObject origin = GameObject.Find(name);
          Transform originTransform = origin.transform;
          Vector3 pos = originTransform.position + new Vector3(0,5,0);
          Vector3 cp = Camera.main.transform.position;
          transform.position = (pos + (cp - pos) * distance);
          Transform lookingAt = Camera.main.transform;
          transform.LookAt(2 * transform.position - lookingAt.position);
          transform.SetParent(GameObject.Find("InteractiveElement").transform);
      }

}
