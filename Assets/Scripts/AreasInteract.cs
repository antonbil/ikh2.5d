﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreasInteract : MonoBehaviour {

    public GameObject a1Button;

    public bool isArea1Visible;

    public List<GameObject> area1List = new List<GameObject>();

    void Awake() {
        foreach (GameObject areaObject in GameObject.FindGameObjectsWithTag("Area1")) {
            area1List.Add(areaObject);
            areaObject.SetActive(false);
        }
    }

    public void AreaVisible(bool isAreaVisible, GameObject areaButton, List<GameObject> areaList) {
        if (isAreaVisible) {
            foreach (GameObject areaObject in areaList) {
                areaObject.SetActive(true);
            }
            areaButton.SetActive(true);
        } else if (!isAreaVisible) {
            foreach (GameObject areaObject in areaList) {
                areaObject.SetActive(false);
            }
            areaButton.SetActive(false);
        }
    }
}