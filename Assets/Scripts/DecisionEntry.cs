using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public class DecisionEntry
{
    private const float ButtonWidth = 200f;
    private static IList<Action<string>> _actions;
    private static IList<string> _titles;
    private readonly GameObject _canvas;
    private readonly GameObject _decisionDialogPrefab;
    private readonly GameObject _decisionButtonPrefab;
    private readonly string _title;

    public DecisionEntry(GameObject decisionDialogPrefab, GameObject decisionButtonPrefab, GameObject canvas, string title)
    {
        _actions = new List<Action<string>>();
        _titles = new List<string>();
        _canvas = canvas;
        _decisionDialogPrefab = decisionDialogPrefab;
        _decisionButtonPrefab = decisionButtonPrefab;
        _title = title;
    }
    public void AddItem(string s, Action<string> action){
        _actions.Add(action);
        _titles.Add(s);
    }

    public void Execute()
    {
        //store gameobjects in list to be able to hide them all at once
        List<GameObject> gos = new List<GameObject>();
        //get panel with title from prefabs
        GameObject go = Object.Instantiate(_decisionDialogPrefab, _canvas.transform);
        gos.Add(go);
        TextMeshProUGUI text = go.GetComponentInChildren<TextMeshProUGUI>();
        text.text = _title;
        var panel = go.transform.Find("ButtonPanel");

        // fiddle with width to spread buttons from left to right in dialog
        int titlesCount = _titles.Count;
            
        float startLength = ButtonWidth/2.0f -(titlesCount * ButtonWidth)/2.0f;
        for (int i = 0; i < titlesCount; i++)
        {
            //store current in var; needed to store i inside loop
            int current = i;
            //create button from prefab
            GameObject button = Object.Instantiate(_decisionButtonPrefab, panel.transform);
            float xc = startLength + ButtonWidth * current;
            button.transform.localPosition = new Vector3(xc, 180, 0);
            gos.Add(button);
            button.GetComponentInChildren<Text>().text = _titles[current];
            //set action of button
            button.GetComponent<Button>().onClick.AddListener(() =>
            {
                //hide elements of decision-dialog
                gos.ForEach(x=>x.SetActive(false));
                //execute action
                _actions[current].Invoke(_titles[current]);
            });
        }
    }

    public void ExecuteTest()
    {
        _actions[0].Invoke("test");
    }
}