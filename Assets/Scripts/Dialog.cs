using System.Collections.Generic;
using UnityEngine;

public class Dialog
{
    public JSONNode json;
    public List<string> Roles = new List<string>(); 
    public List<string> Names = new List<string>();
    public List<string> Aliases = new List<string>();
    public List<string> Genders = new List<string>();

    public Dialog(string json)
    {
        this.json = JSON.Parse(json)["dialog"];
        JSONNode role = this.json["roles"];
        for (int i = 0; i < role.Count; i++)
        {
            JSONNode roleItem = role[i];
            Roles.Add(roleItem["role"]);
            Aliases.Add(roleItem["alias"]);
            Names.Add(roleItem["name"]);
            Genders.Add(roleItem["gender"]);
        }

        CurrentYPosition = 50f;
        CurrentYChange = 0f;
        PreviousThought = false;
    }

    /**
         * replace role-names with aliases, and replace all gender-dependant parts
         */
    public string ReplaceRoleName(string text)
    {
        //list of gender-dependant words
        List<string> heShe = new List<string>(new[] { "Hij/Zij", "hij/zij", "Hem/Haar", "hem/haar" });
        //iterate through heShe-list
        heShe.ForEach(x =>
        {
            //first part is male/second part is female word
            string[] heSheParts = x.Split('/');
            //loop through all names of roles
            for (int i = 0; i < Names.Count; i++)
            {
                string name = Names[i];
                string gender = Genders[i];
                string trans = heSheParts[0];
                if (gender.Equals("female"))
                {
                    trans = heSheParts[1];
                }
                    
                //gender-dependant word is prefixed with role-name
                text = text.Replace(name + x, trans);
            }
        });

        //now loop through all role-names to replace them with aliases
        //this can be done now because all gender-dependant expressions have been replaced already
        for (int i = 0; i < Names.Count; i++)
        {
            string name = Names[i];
            string alias = Aliases[i];

            text = text.Replace(name, alias);
        }
        //return text
        return text;
    }

    public string GetName(string role)
    {
        int index = Roles.FindIndex(x => x.Equals(role));
        return Names[index];
    }
    public string GetGender(string role)
    {
        Debug.Log("get role "+role);
        int index = Roles.FindIndex(x => x.Equals(role));
        return Genders[index];
    }

    public string Id => json["id"];

    public string Title => json["title"];

    public float CurrentYPosition { get; set; }
    public float CurrentYChange { get; set; }
    public bool PreviousThought { get; set; }

    public void DoDialog()
    {
        Debug.Log("Do dialog"+Id);
    }

    public int GetPartsLength()
    {
        return json["parts"].Count;
    }

    public JSONNode GetPartDialog(int i)
    {
        return json["parts"][i];
    }
}