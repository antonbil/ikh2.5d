﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Assets.Scripts;
using TMPro;
using Unity.VectorGraphics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DialogInteract : MonoBehaviour
{
    public static DialogInteract instanceDialogInteract;

    private static bool first = true;
    //store instance of DialogInteract so it can be used in other cs-scripts
    public Dialogs DialogsInstance;
    public static GameObject LeftSpeak1;
    public static  GameObject RightSpeak1;
    public static  GameObject LeftThought1;
    public static  GameObject RightThought1;
    public static GameObject DialogPrefab1;
    public static GameObject DecisionDialogPrefab1;
    public static GameObject DecisionButtonPrefab1;
    public GameObject LeftSpeak;
    public GameObject RightSpeak;
    public GameObject LeftThought;
    public GameObject RightThought;
    public GameObject RoomButton;
    public GameObject InteractiveButtonText;
    public GameObject DialogPrefab;
    public GameObject DecisionDialogPrefab;
    public GameObject DecisionButtonPrefab;

    public static GameObject RoomButton1;
    public static GameObject InteractiveButtonText1;
    //parts of play-pause-button
    private GameObject dialogPlay;
    private GameObject dialogPause;
    public static float scaler = 1;

    // Start is called before the first frame update
    void Start()
    {
        //print(" right-thought"+RightThought.name);
    }
    
    void Awake()
    {
        //copy all prefabs to static; so you only have to add them to the DialogInteract in the main scene
        //all other calls to DialogInteract in unity editor can be left empty
        if (first)
        {
            LeftSpeak1 = LeftSpeak;
            RightSpeak1 = RightSpeak;
            LeftThought1 = LeftThought;
            RightThought1 = RightThought;
            RoomButton1 = RoomButton;
            InteractiveButtonText1 = InteractiveButtonText;
            DialogPrefab1 = DialogPrefab;
            DecisionDialogPrefab1 = DecisionDialogPrefab;
            DecisionButtonPrefab1 = DecisionButtonPrefab;
        }

        //store current instance in static variable
        instanceDialogInteract = this;
        first = false;
    }

    public static GameObject GetCanvas()
    {
        return GameObject.Find("Canvas");
    }
    
    //Dialog-parts (gameobjects) of current dialog
    public List<GameObject> DialogInteractParts = new List<GameObject>();
 
    //position of last dialog-element inside viewport
    int posInDialog = 0;
    //number of last dialog-element of dialog
    int dialogElementNumber = 0;
    //current dialog
    private Dialog dialog;
    //elapsed time for timer
    float elapsed = 0f;
    //if dialog_active: timer is fired
    private bool dialog_active = false;
    void Update()
    {
        //check if timer active
        if (!dialog_active)
            return;
        //2 seconds timer
        float secondsTimer = 2f;
        //check if timer-trigger must be fired
        elapsed += Time.deltaTime;
        if (elapsed >= secondsTimer) {
            elapsed = elapsed % 2f;
            FireDialogTrigger();
        }
    }
    void FireDialogTrigger() {
        //check if dialog is ended
        if (dialogElementNumber < dialog.GetPartsLength())
        {
            //dialog not finished; fire next part of dialog
            //check if viewport must be moved down
            GameObject bg = GameObject.Find("DialogContent");
            //display next part of dialog
            posInDialog = DoNextPartDialog(bg, dialog, posInDialog, dialogElementNumber);
            if (posInDialog > DialogInteractPart.MinSpeakElementsInFrameDialogBeforeScrollUp)
            {
                Vector3 v = bg.transform.localPosition;
                bg.transform.localPosition = new Vector3(v.x, v.y + dialog.CurrentYChange, v.z);
            }
            //increase number of highest visible dialog-element
            dialogElementNumber++;
            instanceDialogInteract.DialogInteractPartsList.ForEach(x =>
            {
                if (!x.InFrame(posInDialog))
                    x.InteractPartGameObject.SetActive(false);
            });
        }
        else
        {
            //dialog ended
            //deactivate timer
            dialog_active = false;
        }
    }
    
    /**
     * find game-object inside parent; also works for inactive game-objects
     */
    public static GameObject FindObject(GameObject parent, string name)
    {
        Component[] trs= parent.GetComponentsInChildren(typeof(Transform), true);
        foreach(Transform t in trs){
            if(t.name == name){
                return t.gameObject;
            }
        }
        return null;
    }

    public static GameObject FindInDialog(string name)
    {
        GameObject instObj1 = GameObject.Find("DialogBackground");
        print("search inside DialogBackGround for "+name);
        return FindObject(instObj1, name);
    }
    public static GameObject FindInDialogButtons(string name)
    {
        GameObject instObj1 = GameObject.Find("DialogButtons");
        print("search inside DialogBackGround for "+name);
        return FindObject(instObj1, name);
    }
    public static GameObject FindContent(string name)
    {
        GameObject instObj1 = GameObject.Find("DialogContent");
        return FindObject(instObj1, name);
    }

    private GameObject canvasUIDialog;
    private List<DialogInteractPart> DialogInteractPartsList = new List<DialogInteractPart>();

    /**
     * initialize dialog, and display it on UI
     */
    public void DoDialog(string dialogName, GameObject BackgroundDialog)
    {
        DialogsInstance = new Dialogs();
        
        //find button-elements in current scene
        dialogPlay = GameObject.Find("DialogPlay");
        dialogPause = GameObject.Find("DialogPause");

        //crude way to scale dialog; based on size of screen
        //should be optimized
        int height = Screen.height;
        int width = Screen.width;
        scaler = width/800f;

        if (height / 600f < scaler)
        {
            scaler = height / 600f;
        }

        if (scaler > 1)
            scaler *= 1.3f;

        //Debug.Log("screen width "+width);
        if (canvasUIDialog == null)
        {
            canvasUIDialog = GameObject.Find("Canvas");
        }
        canvasUIDialog.GetComponent<CanvasScaler>().scaleFactor = scaler;
        
        //find main parts/panels of dialog; reposition if necessary
        //order is important: buttons on top; content in middle
        GameObject dialogObject = FindObject(canvasUIDialog, "Dialog");
        if (dialogObject == null)
        {
            dialogObject = Instantiate(DialogPrefab1, canvasUIDialog.transform);
        }
        GameObject dialogBackgroundObject = FindObject(dialogObject, "DialogBackground");
        //set variable content of dialog to original position
        FindObject(canvasUIDialog, "DialogContent").transform.localPosition = new Vector3(15, 0, 0);
        GameObject dialogButtonsObject = FindObject(canvasUIDialog, "DialogButtons");

        //set controlling buttons; part of dialogButtonsObject
        //first deal with play-pause-button
        GameObject playPauseObject = FindObject(dialogButtonsObject, "PlayPauseButton");
        dialogPause = FindObject(playPauseObject, "PlayPausePlayImage");
        //store in variable dialogPlay so it can be used elsewhere
        dialogPlay = playPauseObject;
        //add action to play-pause-button
        Button playPauseButton = playPauseObject.GetComponent<Button>();
        playPauseButton.onClick.AddListener(() =>
        {
            print("playpause clicked");
            ExecutePlayPauseButton();
        });
        //do the same for close-button
        GameObject closeDialogObject = FindObject(dialogButtonsObject, "CloseDialogRealButton");
        //add action to close-button
        Button closeDialogButton = closeDialogObject.GetComponent<Button>();
        closeDialogButton.onClick.AddListener(() =>
        {
            print("closebutton is clicked");
            ExecuteCloseButton();
        });

        //make background and buttons visible (middle layer always visible, it is empty anyway at start)
        dialogBackgroundObject.SetActive(true);
        dialogButtonsObject.SetActive(true);

        //store canvas as background, so it can be switched on/off at will
        BackgroundDialog = canvasUIDialog;
        RoutesInteract.instanceRoutesInteract.BackgroundDialog = BackgroundDialog;

        //(re-)initialize dialog (middle layer of dialog); clear items if already displayed
        dialog = instanceDialogInteract.DialogsInstance.dialogs[dialogName];
        DialogInteractParts.ForEach(x=>x.SetActive(false));
        DialogInteractParts.Clear();
        DialogInteractPartsList.Clear();
        posInDialog = 0;
        dialogElementNumber = 0;
        //setup main visible elements (persons and buttons)
        BackgroundDialog.SetActive(true);

        //cameras persons
        GameObject camerasPersons = FindInDialog("CameraImages");
        camerasPersons.SetActive(true);
        instanceDialogInteract.DialogInteractParts.Add(camerasPersons);
        //first role
        int roleNumber = 0;
        var gender = GetGender(roleNumber);
        var title = dialog.Title;
        GameObject titleObject = FindObject(canvasUIDialog, "dialogTitle");
        TextMeshProUGUI text = titleObject.GetComponentInChildren<TextMeshProUGUI>();
        text.SetText(title);

        //var baseInst = FindInDialog(gender);
        //GameObject instObj = Instantiate(baseInst);
        /*instObj.name = "left-dialog-object";
        Transform layerTransform = instObj.transform;
        layerTransform.SetParent(dialogBackgroundObject.transform, false);
        //instObj.SetActive(true);
        //add to visible objects of dialog
        instanceDialogInteract.DialogInteractParts.Add(instObj);
        //z = -0.1 to put it before background
        Vector3 lp = layerTransform.transform.localPosition;
        layerTransform.transform.localPosition = new Vector3(-140f,-35f,0);
        //second role
        baseInst = FindInDialog(GetGender(1));
        instObj = Instantiate(baseInst);
        instObj.name = "right-dialog-object";
        layerTransform = instObj.transform;
        layerTransform.SetParent(dialogBackgroundObject.transform, false);
        //second character has same y/z pos as first character
        layerTransform.transform.localPosition = new Vector3(140f,-35f,0);
        //instObj.SetActive(true);
        //add to visible objects of dialog
        instanceDialogInteract.DialogInteractParts.Add(instObj);*/
        
        //activate timer
        dialog_active = true;
        //make main menu unresponsive
        Situations.StaticSituations.DeActivateColliders();
        
    }

    /**
     * get gender of left/right role in dialog
     */
    private string GetGender(int roleNumber)
    {
        string gender = "Child2";
        var dialogRole = dialog.Roles.ElementAt(roleNumber);
        if (dialog.GetGender(dialogRole).Equals("female"))
        {
            gender = "Child1";
        }

        return gender;
    }

    private static int DoNextPartDialog(GameObject BackgroundDialog, Dialog dialog, int posInDialog,
        int dialogElementNumber)
    {
        posInDialog++;
        var dialogPart = dialog.GetPartDialog(dialogElementNumber);
        var jsonNode = dialogPart["role"];
        var leftRightPosition = DialogInteractPartKind.Left;
        int actor = 1;
        var speakKind = DialogInteractPartAct.Speak;
        if (!GetStringValue(jsonNode).Equals("IKH PmK"))
        {
            actor = 2;
            leftRightPosition = DialogInteractPartKind.Right;
        }

        string tagSpeak = GetStringValue(dialogPart["tag"]);
        if (tagSpeak.Equals("thought"))
        {
            speakKind = DialogInteractPartAct.Thought;
        }

        //deal with successive thoughts
        if (speakKind == DialogInteractPartAct.Thought && dialog.PreviousThought)
        {
            dialog.CurrentYPosition = dialog.CurrentYPosition - DialogInteractPart.SizeDialogElement;
            dialog.CurrentYChange = DialogInteractPart.SizeDialogElement;
            posInDialog++;
            //workaround; must be improved....
            //posInDialog++;
        }

        DialogInteractPart dialogInteractPart = new DialogInteractPart(dialog, BackgroundDialog.transform, dialogPart["content"], posInDialog, leftRightPosition, speakKind, scaler, actor, posInDialog);
        var dialogPartGameObject = dialogInteractPart.InteractPartGameObject;
        //add to all gameobjects of this dialog
        instanceDialogInteract.DialogInteractParts.Add(dialogPartGameObject);
        instanceDialogInteract.DialogInteractPartsList.Add(dialogInteractPart);
        //if thought; stay at same line
        if (tagSpeak.Equals("thought"))
        {
            posInDialog--;
        }

        return posInDialog;
    }

    /**
     * helper function to cast value of jsonnode to string
     */
    private static string GetStringValue(JSONNode jsonNode)
    {
        return jsonNode.ToString().Replace("\"", "");
    }

    public void ExecutePlayPauseButton()
    {
        //play-pause button clicked
        SVGImage image = dialogPlay.GetComponent<SVGImage>();
        bool working = image.IsActive();
        image.enabled = !working;
        dialogPause.SetActive(working);
        //if play-button visible: stop timer
        dialog_active = !working;
    }

    public void ExecuteCloseButton()
    {
        //close current dialog; restore main dialog(s)
        dialog_active = false;
        RoutesInteract.instanceRoutesInteract.BackgroundDialog.SetActive(false);
        Situations.StaticSituations.ActivateColliders();

    }
}
