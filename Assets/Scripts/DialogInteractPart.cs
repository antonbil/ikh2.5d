using System;
using TMPro;
using Unity.VectorGraphics;
using UnityEngine;

public enum DialogInteractPartKind
{
    Left,
    Right
}
public enum DialogInteractPartAct
{
    Thought,
    Speak
}
public class DialogInteractPart
{
    private const int MaxSpeakElementsInFrameDialog = 5;
    public const int MinSpeakElementsInFrameDialogBeforeScrollUp = 4;
    public const float SizeDialogElement = 60f;

    private readonly int _posInDialog;

    public bool InFrame(int largestPositionInDialog)
    {
        int realPositionInDialog = _posInDialog;
        if (Thought == DialogInteractPartAct.Thought)
        {
            //thought-part on same line with next speak-object
            realPositionInDialog++;
        }

        return realPositionInDialog > largestPositionInDialog - MaxSpeakElementsInFrameDialog;
    }

    public GameObject InteractPartGameObject { get;}

    public DialogInteractPart(Dialog dialog, Transform parentTransform, string text, int y, DialogInteractPartKind dk, DialogInteractPartAct act, float scaler, int actor, int posInDialog)
    {
        this._posInDialog = posInDialog;
        //there are four hidden gameobjects with names: LeftSpeak, RightSpeak, LeftThought and RightThought
        Dialog = dialog;
        text = Dialog.ReplaceRoleName(text);
        string speak = "LeftSpeak";
        string newSpeak = "IKH_GESPREK1";
        var x = -10f;
        //color of text-speak is almost black, but entirely....
        Color32 color = new Color32(10, 10, 10, 255);
        int rightThoughtCompensation = 0;
        if (dk != DialogInteractPartKind.Left)
        {
            speak = "RightSpeak";
            newSpeak = "IKH_GESPREK2";
            //position on right side; becomes positive for right speak and thought
            x = 30f;
            rightThoughtCompensation = (int) (x - 10);
        }

        Thought = DialogInteractPartAct.Thought;

        if (act == DialogInteractPartAct.Thought)
        {
            speak = speak.Replace("Speak", "Thought");
            newSpeak = newSpeak.Replace("GESPREK", "GEDACHTEN");
            x = x * scaler * 10f;
            //min width = half width speak + half width thought + 5
            int minWidth = 120 + 75 + 5;
            if (speak.Contains("Right"))
            {
                minWidth += rightThoughtCompensation;
            }

            float xRightThoughtCompensation = 0.0f;
            if (dk == DialogInteractPartKind.Right)
            {
                //@Tom:
                //set this to a negative value (-15.0f perhaps?) if right-thought is not at correct position and thought must be positioned more to the left
                xRightThoughtCompensation = 0.0f;
            }

            x = Math.Sign(x)*minWidth+xRightThoughtCompensation;

            //set color of thought to white
            color = new Color32(255, 255, 255, 255);
        }

        //speak += actor;
        newSpeak = "COPY" + actor + newSpeak;
        Sprite sprite = Resources.Load<Sprite>("DialogSprites/"+newSpeak);
        Debug.Log("load sprite "+newSpeak);
        //find hidden gameobject in dialog to instantiate (acts as prefab)
        GameObject hiddenGameObject;
        switch (speak)
        {
            case "RightThought":

            Debug.Log("get rightthought from prefab");
            hiddenGameObject = DialogInteract.RightThought1;
            break;
            case "LeftThought":
                hiddenGameObject = DialogInteract.LeftThought1;
                break;
            case "LeftSpeak":
                hiddenGameObject = DialogInteract.LeftSpeak1;
                break;
            case "RightSpeak":
                hiddenGameObject = DialogInteract.RightSpeak1;
                break;
            default:
                hiddenGameObject = DialogInteract.FindContent(speak);
                break;
        }

        //instantiate object and set its parent
        var newSpeakGameObject = UnityEngine.Object.Instantiate(hiddenGameObject);
        newSpeakGameObject.name = "dialog-object-"+speak+y;
        SVGImage svgImage = newSpeakGameObject.GetComponentInChildren<SVGImage>();
        svgImage.sprite = sprite;
        var newSpeakGameObjectTransform = newSpeakGameObject.transform;
        newSpeakGameObjectTransform.SetParent(parentTransform, true);
        
        //set text and color of TextMesh
        var textMesh = newSpeakGameObject.GetComponentInChildren<TextMeshProUGUI>();
        textMesh.text = text;
        textMesh.color = color;
        
        //set position and scale of bubble
        //initial position is somewhere (100px) below middle y
        float yScale = 1.0f;
        if (text.Length < 80)
            yScale = 0.8f;
        if (text.Length < 50)
            yScale = 0.5f;
        //scale original; sometimes it is changed when surrounding Canvas changes scale
        Vector3 position = new Vector3(x,dialog.CurrentYPosition - SizeDialogElement * yScale/2,-0.1f);
        if (act != DialogInteractPartAct.Thought)
        {
            dialog.CurrentYPosition = dialog.CurrentYPosition - SizeDialogElement * yScale;
            dialog.CurrentYChange = SizeDialogElement * yScale;
        }

        dialog.PreviousThought = act == DialogInteractPartAct.Thought;

        newSpeakGameObjectTransform.localPosition = position;
        newSpeakGameObjectTransform.localScale = new Vector3(1,yScale,1);
        newSpeakGameObjectTransform.GetChild(0).localScale = new Vector3(1,1/yScale,1);
        
        //set return object
        InteractPartGameObject = newSpeakGameObject;
        //make game-object visible
        InteractPartGameObject.SetActive(true);

    }

    private DialogInteractPartAct Thought { get; set; }

    private Dialog Dialog { get; set; }
}