using System.Collections.Generic;
using System.Runtime.CompilerServices;

public class Dialogs
{
    public Dictionary<string,Dialog> dialogs = new Dictionary<string,Dialog>();

    public Dialogs()
    {
        var text = Utility.LoadResourceTextfile("dialog_IKH_Dialog_script_2_S1-2.json");
        Dialog dialog = new Dialog(text);
        dialogs.Add("IKH_Dialog_script_2_S1-2", dialog);
        text = Utility.LoadResourceTextfile("dialog_IKH_Dialog_script_1_S1-2.json");
        dialog = new Dialog(text);
        dialogs.Add("IKH_Dialog_script_1_S1-2", dialog);
    }
        
}