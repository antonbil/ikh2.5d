﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class InstanceObject : MonoBehaviour {

    //New Game Object, Transform, SpriteRenderer, Text, (Other Object)
    public List<GameObject> stationObjList = new List<GameObject>();
    public List<GameObject> niveauObjList = new List<GameObject>();
    public List<GameObject> instNiveauObjList = new List<GameObject>();
    public List<Vector3> posList = new List<Vector3>();
    public static InstanceObject MyGame = null;

    //public List<GameObject> transformObjList = new List<GameObject>();
    private GameObject parent;

    private GameObject instObj;

    private GameObject foreground;
    private GameObject middleground;
    private GameObject background;
    private Situations situations;

    void grid()
    {
        int vertical = (int)Camera.main.orthographicSize - 1;
        int horizontal = vertical * Screen.width / Screen.height;
        int column = horizontal * 2;
        int row = vertical * 2;
        int[, , ,] grid = new int[column, row, horizontal,vertical];
        print("grid element"+row + ","+column + ","+vertical + ","+horizontal + ","+Screen.height + ","+Screen.width + ",");
        /*for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                grid[i, j] = Random.Range(0, 10);
                //SpawnTile(i, j, grid[i, j]);
            }
        }*/

    }

    //Laad alle objecten uit de resources folder en spawn ze als instanced object.
    void Awake()
    {
        MyGame = this;
        foreach (GameObject stationObj in Resources.LoadAll("Situaties", typeof(GameObject))) {
            stationObjList.Add(stationObj);
            stationObj.tag = "Situatie";
        }

        foreach (GameObject niveauObj in Resources.LoadAll("Niveau2", typeof(GameObject))) {
            niveauObjList.Add(niveauObj);
            niveauObj.tag = "SituatieCard";
        }

        foreach (GameObject layerObj in Resources.LoadAll("Layers", typeof(GameObject))) {
            SpawnParentObj(layerObj);
        }

        grid();
        situations = new Situations();
    }


    void Start() {
        parent = new GameObject("Stations");
        //Voor de lengte van de SituatieObj lijst, spawn Situaties en Niveau 2 schilden
        for (int i = 0; i < stationObjList.Count; i++) {
            Vector3 posV3;
            //SpawnObject(stationObjList[i], out posV3);
            //spawnNiveauObject(niveauObjList[i], posV3);
        }
        situations.DisplayTextNodes();
    

    }




    //Maak de layer objecten gelijk aan de utility objecten zodat ze makkelijker aangeroepen kunnen worden.
    public void SpawnParentObj(GameObject parentObj) {
        GameObject instParentObj = Instantiate(parentObj);
        instParentObj.name = parentObj.name;
        if (instParentObj.name.Contains("Fore")) {
            Utility.foreground = instParentObj;
        }
        if (instParentObj.name.Contains("Middle")) {

            Utility.middleground = instParentObj;
        }
        if (instParentObj.name.Contains("Back")) {
            Utility.background = instParentObj;
        }
    }

    //Spawn Niveau2 schild; Naam van originele object, + Set layer object, +Set InstSituatieObj als parent
    public void spawnNiveauObject(GameObject spawnObj, Vector3 posV3) {
        GameObject niveauObj = Instantiate(spawnObj);
        niveauObj.SetActive(false);
        instNiveauObjList.Add(niveauObj);
        niveauObj.name = spawnObj.name;
        niveauObj.transform.SetParent(Utility.foreground.transform, true);
        niveauObj.transform.position = new Vector3(posV3.x, posV3.y, .01f);
    }



}