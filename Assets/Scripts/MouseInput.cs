﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInput : MonoBehaviour {

    /*public SituatieInteract sitInt;
    public InstanceObject instScript;
    public RoutesInteract routesInt;
    public ThemaInteract themaInt;
    public AreasInteract areasInt;*/
    public Camera cam;

    public RaycastHit2D rayHit;

    private bool isSituatie;

    void Update()
    {
        Vector3 mousePos = Input.mousePosition;

        mousePos.z = 10;

        Vector3 screenPos = cam.ScreenToWorldPoint(mousePos);

        //Mouse krijgt raycast om te checken of het iets raakt
        rayHit = Physics2D.Raycast(screenPos, Vector2.zero);
        if (rayHit.collider != null) {
                

                //Mouse collide met Situatie schiuld, als mouseclick pin Niveau 2 schild.
                if (rayHit.collider.CompareTag("Situatie"))
                {
                    //sitInt.HoverandPin(rayHit.collider.transform);
                    if (Input.GetMouseButtonDown(0))
                    {
                        //situation triggered, pass trigger to Situations to deal with it
                        Situations.StaticSituations.ExecuteNode(screenPos.x, screenPos.y, rayHit.collider.name);

                        //sitInt.isPinned = !sitInt.isPinned;
                    }

                    isSituatie = true;
                }

                //Mouse collide met Route button, als mouseclick activeer die route.
                if (rayHit.collider.name.StartsWith("Route") && Input.GetMouseButtonDown(0))
                {
                    print("i will execute route-execute");
                    RoutesInteract.instanceRoutesInteract.ExecuteRoute(rayHit.collider.name);
                }

                if (rayHit.collider.name.StartsWith("Area") && Input.GetMouseButtonDown(0))
                {
                    print("i will execute area-execute");
                    RoutesInteract.instanceRoutesInteract.ExecuteArea(rayHit.collider.name);
                }

            /*if (rayHit.collider.CompareTag("Route1Button")) {
                if (Input.GetMouseButtonDown(0)) {
                    routesInt.RouteActivate(0);
                }
            }

            if (rayHit.collider.CompareTag("Route2Button")) {
                if (Input.GetMouseButtonDown(0)) {
                    routesInt.RouteActivate(1);
                }
            }

            if (rayHit.collider.CompareTag("Route3Button")) {
                if (Input.GetMouseButtonDown(0)) {
                    routesInt.RouteActivate(2);
                }
            }*/
            //Mouse collide met Thema button, als mouseclick activeer die thema.
            /*if (rayHit.collider.CompareTag("AngstButton")) {
                if (Input.GetMouseButtonDown(0)) {
                    themaInt.isAngstVisible = !themaInt.isAngstVisible;
                    themaInt.ThemaVisible(themaInt.isAngstVisible, themaInt.AngstList);
                }
            }

            if (rayHit.collider.CompareTag("ConflictButton")) {
                if (Input.GetMouseButtonDown(0)) {
                    themaInt.isConflictVisible = !themaInt.isConflictVisible;
                    themaInt.ThemaVisible(themaInt.isConflictVisible, themaInt.ConflictList);
                }
            }

            //Mouse collide met Area button, als mouseclick activeer die area.
            if (rayHit.collider.CompareTag("Area1Button")) {
                if (Input.GetMouseButtonDown(0)) {
                    areasInt.isArea1Visible = !areasInt.isArea1Visible;
                    areasInt.AreaVisible(areasInt.isArea1Visible, areasInt.a1Button, areasInt.area1List);
                }
            }*/

        } else {
            //Als mouse niet meer over collider hovered, check of situatie gepinned moet worden
            if (isSituatie) {
                //sitInt.HoverExit();
                isSituatie = false;
            }
        }
    }
}