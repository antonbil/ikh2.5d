﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.Playables;
using UnityEngine.Timeline;


public class Player : MonoBehaviour {

    public GameObject mainCam;
    public Transform camMovePos;
    public GameObject buttons;
    public LayerMask clickRoom;
    private NavMeshAgent myAgent;

    private Animator animator;
    private bool isWalking;

    public GameObject startLocation;

    public PlayableDirector director;

    void Start() {
        myAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            Ray myRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;


            if (Physics.Raycast(myRay, out hitInfo))
            {
                //this one catches collision3d-object
                print("check for single raycast");
                if (Roles.RolesInstance.IsMessage(hitInfo))
                {
                    //temporarily do dialog
                    myAgent.SetDestination(startLocation.transform.position);
                    //DialogInteract.instanceDialogInteract.DoDialog("IKH_Dialog_script_2_S1-2", RoutesInteract.instanceRoutesInteract.BackgroundDialog);
                    return;
                }
            }

            /*if (!EventSystem.current.IsPointerOverGameObject()) {
                if (Physics.Raycast(myRay, out hitInfo, 500, clickRoom)) {

                    myAgent.SetDestination(hitInfo.point);
                }
            }*/
        }

        if (myAgent.velocity != Vector3.zero) {
            animator.SetBool("isWalking", true);
        } else {
            animator.SetBool("isWalking", false);
        }
    }

    private void OnTriggerEnter(Collider other) {
        print("on trigger enter "+other.tag);
        if (other.tag == "cutsceneTrigger") {
            buttons.SetActive(false);
            StartCoroutine(MoveCameraToPosition(mainCam.transform, camMovePos, 2f));
        }
    }

    public void Button1() {
        myAgent.SetDestination(startLocation.transform.position);
    }

    public IEnumerator MoveCameraToPosition(Transform _cameraToMove, Transform _locationToMoveTo, float _timeToMove) {
        Vector3 currentPos = _cameraToMove.transform.position;
        float t = 0f;

        while (t < 1) {
            t += Time.deltaTime / _timeToMove;
            _cameraToMove.transform.position = Vector3.Lerp(currentPos, _locationToMoveTo.position, t);
            _cameraToMove.rotation = Quaternion.Lerp(_cameraToMove.rotation, _locationToMoveTo.rotation, t * .1f);
            yield return null;
        }

        print(" start director play");
        //does not play cut-scene anymore!
        StartCutscene();
    }

    private void StartCutscene() {
        //Debug.Log("Binding Timeline Tracks!");
        //var timelineAsset = (TimelineAsset)director.playableAsset;
        // iterate through tracks and map the objects appropriately
        //print(" timeline " + timelineAsset);
        /*for( var i = 0; i < trackList.Count; i ++)
        {
            if( trackList[i] != null)
            {
                var track = (TrackAsset)timelineAsset.outputs[i].sourceObject;
                timeline.SetGenericBinding(track, trackList[i]);
            }
        }*/
        director.Play();
    }
}