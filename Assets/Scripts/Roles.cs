using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Assets.Scripts
{
    public class Roles
    {
        public static Roles RolesInstance;
        public static GameObject ObjectToWalkTo;
        private readonly GameObject _caller;
        private static readonly Dictionary<string, UnityAction> _buttonActions =new Dictionary<string, UnityAction>();

        public bool IsMessage(RaycastHit hitInfo)
        {
            MonoBehaviour.print("check for"+hitInfo.collider.name);
            if (_buttonActions.ContainsKey(hitInfo.collider.name))
            {
                _buttonActions[hitInfo.collider.name].Invoke();
                return true;
            }
            return false;
        }

        public Roles(GameObject caller = null)
        {
            _caller = caller;
            RolesInstance = this;
            GameObject person1 = GameObject.Find("Josh");
            GameObject person2 = GameObject.Find("Martha");
            try
            {
                //modify appearance of Josh and Martha based on their setup; clothes; age; face
                List<GameObject> markerObjects = new List<GameObject>();
                markerObjects.Add(SetMarkerDialog("Gesprek met leidinggevende", "Table_OfficeDesk_center"));
                markerObjects.Add(SetMarkerDialog("", "Cubicle_laptop"));
                //set the same distance to the camera for all markers
                //set min to large number
                float minDistance = 10000f;
                //store position of camera
                Vector3 cameraPosition = Camera.main.transform.position;
                markerObjects.ForEach(x =>
                {
                    Vector3 markerPosition = x.transform.position;
                    float differenceVector = (cameraPosition - markerPosition).magnitude;
                    if (minDistance > differenceVector)
                    {
                        minDistance = differenceVector;
                    }
                });
                markerObjects.ForEach(x =>
                {
                    Vector3 markerPosition = x.transform.position;
                    Vector3 v = (cameraPosition - markerPosition);
                    float currentDistance = (cameraPosition - markerPosition).magnitude;
                    //set magnitude of difference vector to minDistance
                    v = v * minDistance / currentDistance;
                    //set marker-object at new distance
                    x.transform.position = (cameraPosition - v);

                });

            }
            catch (Exception e)
            {
                MonoBehaviour.print("error copying persons"+e.ToString());
            }
            //Cubicle_laptop
        }

        private GameObject SetMarkerDialog(string text, string name)
        {
            try
            {

                GameObject button = DialogInteract.RoomButton1;//UnityEngine.GameObject.Find("RoomButton");
                var button1Name = button.name + "_button";
                UnityAction buttonAction = () => { Debug.Log("I am clicked"); };
                try
                {
                    _buttonActions.Add(button1Name, buttonAction);
                }
                catch (Exception e)
                {
                    _buttonActions[button1Name] = buttonAction;
                }
                GameObject button1 = Object.Instantiate(button);
                button1.name = button1Name;
                button1.gameObject.AddComponent<AlwaysFacingCameraInfoObject>();
                AlwaysFacingCameraInfoObject cp = button1.GetComponent<AlwaysFacingCameraInfoObject>();
                MonoBehaviour.print("add button"+button1.name);
                button1.GetComponent<Button>().onClick.AddListener(buttonAction);
                ;
                cp.SetDistance(name, 0.4f);
                cp.SetOrigin(name);
                //InteractiveButtonText
                GameObject myText = DialogInteract.InteractiveButtonText1;//UnityEngine.GameObject.Find("InteractiveButtonText");
                GameObject myText1 = Object.Instantiate(myText);
                AlwaysFacingCameraInfoObject missingScript = myText1.gameObject.AddComponent<AlwaysFacingCameraInfoObject>();
                
                myText1.name = name + "_text";
                buttonAction = () =>
                {
                    Debug.Log("I am clicked (text)"); 
                    myText1.SetActive(false);
                    button1.SetActive(false);
                    ObjectToWalkTo = GameObject.Find(name);
                };
                MonoBehaviour.print("add text"+myText1.name);
                try
                {
                    _buttonActions.Add(myText1.name, buttonAction);
                }
                catch (Exception e)
                {
                    _buttonActions[myText1.name] = buttonAction;
                }
                //_buttonActions.Add(myText1.name, buttonAction);
                cp = myText1.GetComponent<AlwaysFacingCameraInfoObject>();
                cp.SetDistance(name, 0.5f);
                //do not display text on button; is donw on top of dialog itself
                //cp.SetText(text);
                return button1;
            }
            catch (Exception e)
            {
                MonoBehaviour.print("error creating" + name + e.ToString());
            }

            return null;
        }
    }
}