﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RoutesInteract : MonoBehaviour
{
    public static float MiddleLayerPosition = 0.99f;
    public List<GameObject> routeList = new List<GameObject>();
    public List<GameObject> areasList = new List<GameObject>();
    public GameObject situationDisplay = null; 

    public static RoutesInteract instanceRoutesInteract;
    //elements to store state of buttons
    private readonly Dictionary<string,GameObject> _okElements =new Dictionary<string, GameObject>();
    private readonly bool _legendPanelLeft = true;
    public GameObject BackgroundDialog = null;
    private float _aspect = 1.0f;

    void Awake()
    {
        instanceRoutesInteract = this;
        
        //use R1Button as a kind of prefab
        var baseInst = GameObject.Find("R1Button");
        
        _aspect = 6.097733f / 5.092936f;

        //get background-layers from Resources-folder; create button for each of them if Route or Area
        foreach (var o in Resources.LoadAll("Backgrounds", typeof(GameObject)))
        {
            var routeObj = (GameObject) o;
            CreateBackgroundLayer(routeObj, baseInst);
        }

            //make R1Button invisible; R1Button not needed anymore
        //baseInst.SetActive(false);
        GameObject buttons = GameObject.Find("Buttons");
        //alternative position for legend-buttons?
        if (!_legendPanelLeft)
        {
            buttons.transform.localPosition = new Vector3(33,25,0);
        }

        //set first route as default
        ExecuteRoute("Route1button");
        //for state of Route1button to visible (buttons always visible...)
        SetStateButton("Route1button", buttons);
    }


    //create layer for element in Resource-folder
    //Routelist; voor elke verschillende route voeg een tag toe
    private void CreateBackgroundLayer(GameObject spawnObj, GameObject baseInst)
    {
        GameObject instObj = Instantiate(spawnObj);
        instObj.name = spawnObj.name;
        Transform layerTransform = instObj.transform;
        layerTransform.SetParent(Utility.background.transform, true);
        //layer for object (non-route and non-area) in fore-ground
        layerTransform.position = new Vector3(32, 18, MiddleLayerPosition);

        SpriteRenderer sr = instObj.GetComponent<SpriteRenderer>();

        if (Camera.main is null) return;
        float worldScreenHeight = Camera.main.orthographicSize * 2;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        var parentOfButtons = GameObject.Find("Buttons");

        var sprite = sr.sprite;
        float screenHeight = worldScreenHeight / sprite.bounds.size.y;
        float screenWidth = screenHeight*_aspect;
        layerTransform.localScale = new Vector3(
            screenWidth,
            screenHeight, 1);


        //left middle
        int base_y = -7;
        float base_x = -13.805f;

        bool backgroundFound = false;
        int maxRoutes = 20;
        //check for Route1..Route7 inside Resources-dir
        for (int i = 1; i < maxRoutes; i++)
        {
            if (!instObj.name.Contains("Route" + i)) continue;
            backgroundFound = true;
            // route1List.Add(instObj);
            //change z-position to put in right layer
            layerTransform.position = new Vector3(32, 18, 1.0f * i);
            //only first Route is visible by default
            bool active = i == 1;
            instObj.SetActive(active);
            //add to route-list
            routeList.Add(instObj);

            //button on next row
            int y = base_y - i;
            //x-pos for routes
            float x = -3.0f;
            string baseButtonName = GetRouteButtonName(i);
            string text = Situations.StaticSituations.GetRoleDescription(i);
            CreateButton(base_x + x, y, text, baseInst, baseButtonName, parentOfButtons);
        }

        //check for Area1..Area4 inside Resources-dir
        for (int i = 1; i < 20; i++)
        {
            if (!instObj.name.Contains("Area" + i)) continue;
            backgroundFound = true;
            //area behind all 7 routes; 
            //change z-position to put in right layer
            layerTransform.position = new Vector3(32, 18, 1.0f * (maxRoutes + i));
            //areas inactive at start
            instObj.SetActive(false);
            //add to area-list
            areasList.Add(instObj);

            //button on next row
            int y = base_y - i;
            //area-legends 11 right of role-legends
            float x = 8.0f;
            string areaButtonName = GetAreaButtonName(i);
            string areaButtonText = Situations.StaticSituations.GetAreaDescription(i);
            CreateButton(base_x + x, y, areaButtonText, baseInst, areaButtonName, parentOfButtons);
        }

        /*if (instObj.name.Contains("DialogBackground"))
        {
            backgroundFound = true;
            layerTransform.position = new Vector3(32, 18, 0.8f);
            instObj.SetActive(false);
            BackgroundDialog = instObj;
        }*/

        if (!backgroundFound)
        {
             situationDisplay = instObj;
        }
    }

    /**
     * create button with name=baseButtonName and text = text
     */
    private void CreateButton(float x, int y, string text, GameObject baseInst, string baseButtonName, GameObject parent)
    {
        //instantiate and set parent
        var baseButton = Instantiate(baseInst);
        BoxCollider2D boxCollider2D = baseButton.GetComponent<BoxCollider2D>();
        // add to interactive elements of main screen
        Situations.StaticSituations.InteractveElementsMainScreen.Add(boxCollider2D);

        var basePos = baseButton.transform.position;
        basePos.y = y;
        basePos.x = /*base_pos.x +*/ x;
        baseButton.transform.position = basePos;
        baseButton.name = baseButtonName;
        //get text-mesh; store it in list TextElements
        TextMeshPro textElement = baseButton.GetComponentsInChildren<TextMeshPro>()[0];
        var char1 = "";//GetActiveChar(active);

        textElement.text = char1 + "<pos=10.25><color=\"white\">" + text + "</color>";
        textElement.alignment = TextAlignmentOptions.BaselineLeft;
        //get child-element of baseButton with name: OK
        Transform trans = baseButton.transform;
        Transform childTrans = trans. Find("OK");
        //ok-button switched off by default
        GameObject okObject;
        (okObject = childTrans.gameObject).SetActive(false);
        //add it to list, so the state can be switched on/off later-on
        _okElements.Add(baseButtonName, okObject);

        //get sprite-renderer
        //SpriteRenderer sprite = baseButton.GetComponentsInChildren<SpriteRenderer>()[0];
        //sprite.size = new Vector2(20, 1);
        RectTransform tr0 = baseButton.GetComponentsInChildren<RectTransform>()[0];
        tr0.pivot = new Vector2(0, 0.5f);
        tr0.sizeDelta = new Vector2(6, 0.16f);
        RectTransform tr = baseButton.GetComponentsInChildren<RectTransform>()[1];
        tr.sizeDelta = new Vector2(11, 1);
        //set parent of baseButton (do not combine with instantiate; gives side-effects (position??)!)
        // ReSharper disable once Unity.InstantiateWithoutParent
        baseButton.transform.SetParent(parent.transform, false); //add the gameobject to the canvas
        
    }



    //Als een Route button geclicked wordt wordt die route aan/uitgezet.
    private void RouteActivate(int currentIndex, string buttonName)
    {
        GameObject go = routeList[currentIndex];
        var isActive = go.activeSelf;
        go.SetActive(!isActive);
        SetStateButton(buttonName, go);
    }

    /**
     * set state of button
     */
    private void SetStateButton(string buttonName, GameObject go)
    {
        _okElements[buttonName].SetActive(go.activeSelf);
        _okElements[buttonName].transform.SetAsLastSibling ();

    }

    public void ExecuteRoute(string name1)
    {
        for (int i = 1; i < 20; i++)
        {
            string buttonName = GetRouteButtonName(i);
            if (buttonName == name1 && Input.GetMouseButtonDown(0))
            {
                RouteActivate(i - 1, name1);
            }
        }
    }

    /**
     * get default button name based on id
     */
    private static string GetRouteButtonName(int i)
    {
        return "Route" + i + "button";
    }

    public void ExecuteArea(string name1)
    {
        for (int i = 1; i < 20; i++)
        {
            string buttonName = GetAreaButtonName(i);
            if (buttonName == name1 && Input.GetMouseButtonDown(0))
            {
                GameObject area = areasList[i - 1];
                var isActive = area.activeSelf;
                area.SetActive(!isActive);
                SetStateButton(name1, area);
            }
        }
    }

    private static string GetAreaButtonName(int i)
    {
        return "Area" + i + "button";
    }
}
