﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SituatieInteract : MonoBehaviour {
    [HideInInspector] public bool isPinned;
    [HideInInspector] public bool isFirstTime;

    public InstanceObject instScript;

    public List<GameObject> situatiesList = new List<GameObject>();
    public List<GameObject> situatieCardsList = new List<GameObject>();

    public int listNumber;

    // Als de muis over een situatie hovered display Niveau 2 schild, als de muis op een situatie clicked, pin Niveau 2 schild
    public void HoverandPin(Transform other) {
        for (int i = 0; i < instScript.stationObjList.Count; i++) {
            if (instScript.stationObjList[i].name == other.transform.name) {
                if (isFirstTime) {
                    if (!instScript.instNiveauObjList[i].gameObject.activeSelf) {
                        isPinned = false;
                    } else {
                        isPinned = true;
                    }
                    isFirstTime = false;
                }
                listNumber = i;

                instScript.instNiveauObjList[i].gameObject.SetActive(true);
                print(instScript.instNiveauObjList[i]);
            }
        }
    }

    //Check of Niveau 2 schild gepinned is, zo niet dan wordt die weer onzichtbaar
    public void HoverExit() {
        isFirstTime = true;
        if (instScript.instNiveauObjList[listNumber].gameObject.activeSelf && isPinned) {
            instScript.instNiveauObjList[listNumber].gameObject.SetActive(true);
        } else {
            instScript.instNiveauObjList[listNumber].gameObject.SetActive(false);
        }
    }
}