    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Assets.Scripts;
    using TMPro;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public class Situations
    {
        public static Situations StaticSituations = null;
        Dictionary<string,JSONNode> nodes = new Dictionary<string, JSONNode>();
        private JSONNode json;

        public Situations()
        {
            StaticSituations = this;
            var text = Utility.LoadResourceTextfile("example.json");
            json = JSON.Parse(text);
            var nodes1 = json["nodes"];
            for (int i = 0; i< nodes1.Count; i++)  //or res.Count()
            { 
                nodes.Add(nodes1[i]["id"],nodes1[i]);
                //print( "add:"+nodes1[i]["id"]);
            }
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        public string GetAreaDescription(int i)
        {
            return json["zones"][i - 1]["zone"];
        }
        public string GetRoleDescription(int i)
        {
            return json["roles"][i - 1]["name"];
        }
        
        // called second
        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            //when scene is loaded; setup characters appearance etc.
            new Roles();

        }
        
        public void ExecuteNode(float x, float y, string name)
        {
            Debug.Log("execute node"+x+y+name);
            if (nodes.ContainsKey(name))
            {
                Debug.Log("found in situations"+nodes[name]["name"]);
                
            }

            string dialog = "IKH_Dialog_script_1_S1-2";
            if (name.Contains("_S1"))
            {
                dialog = "IKH_Dialog_script_2_S1-2";
            }
            SceneManager.LoadScene("TestWorld");
            

            //scene only loaded in next frame; so do not get load DialogBackground yet
            //DialogInteract.instanceDialogInteract.DoDialog(dialog, RoutesInteract.instanceRoutesInteract.BackgroundDialog);

        }
        float GetGridWidth()
        {
            return /*Screen.width*/63.2f / 9.0f;
        }

        float GetGridHeight()
        {
            return /*Screen.height*/33.8f / 5.0f;
        }
        
        public void DisplayTextNodes()
        {
            foreach (var kv in nodes)
            {
                var x = kv.Value["start"][0].AsInt;
                var y = kv.Value["start"][1].AsInt;
                var desc = kv.Value["name"];
                var id = kv.Value["id"];
                if (x < 20 && y < 20)
                {
                    //Debug.Log("convert?" + desc + "," + kv.Key + x + "," + y);
                    Vector3 posV3 = new Vector3(20, 20, 0.02f);
                    string name = "sit" + id;
                    try
                    {
                        //print("look for node:" + spawnObj.name);
                        //JSONNode node = nodes[id];
                        //name = node["name"];
                        //print("node found:" + node["id"]+node["start"][0]);
                        posV3.x = 0.8f + (-0.58f + x) * GetGridWidth();
                        posV3.y = 1.5f + (5.55f - y) * GetGridHeight();
                    }
                    catch (Exception e)
                    {
                    }

                    GameObject instObj = new GameObject(); //Instantiate(spawnObj);
                    instObj.tag = "Situatie";
                    var dCollider = instObj.AddComponent<BoxCollider2D>();
                    dCollider.size = new Vector2(3.5f, 3.5f);
                    instObj.name = id;
                    instObj.transform.SetParent(Utility.middleground.transform, true);
                    instObj.transform.position = new Vector3(posV3.x, posV3.y, .02f);
                    instObj.transform.localScale = new Vector3(2, 2, 2);
                    BoxCollider2D boxCollider2D = instObj.GetComponent<BoxCollider2D>();
                    this.InteractveElementsMainScreen.Add(boxCollider2D);
                    AddText(desc, instObj);
                }
            }

            // set MiddleGround position layer AFTER positions text-objects have been set. 
            GameObject instParentObj = Utility.middleground;
            instParentObj.transform.position = new Vector3(-5f,0f,0.02f);
            instParentObj.transform.localScale = new Vector3(1.16f, 1, 1);

        }

        public List<BoxCollider2D> InteractveElementsMainScreen = new List<BoxCollider2D>();

        IEnumerable<string> SplitToLines(string stringToSplit, int maximumLineLength)
        {
            var words = stringToSplit.Split(' ').Concat(new [] { "" });
            return
                words
                    .Skip(1)
                    .Aggregate(
                        words.Take(1).ToList(),
                        (a, w) =>
                        {
                            var last = a.Last();
                            while (last.Length > maximumLineLength)
                            {
                                a[a.Count() - 1] = last.Substring(0, maximumLineLength);
                                last = last.Substring(maximumLineLength);
                                a.Add(last);
                            }
                            var test = last + " " + w;
                            if (test.Length > maximumLineLength)
                            {
                                a.Add(w);
                            }
                            else
                            {
                                a[a.Count() - 1] = test;
                            }
                            return a;
                        });
        }

        
        //Text die op Situatie schild komt; +Centered, +Font Size, +Set InstSituatieObj als Parent
        public void AddText(string name, GameObject instObj)
        {
            for (int i = 1; i < 25; i++)
            {
                string s = "(S" + i + ")";
                name = name.Replace(s, "");
            }
            IEnumerable<string> lines = SplitToLines(name, 18);
            name = String.Join("\n",lines);
            GameObject textObj = new GameObject("Text");
            TextMeshPro text = textObj.AddComponent<TMPro.TextMeshPro>();
            text.SetText(name);
            text.alignment = TextAlignmentOptions.Center;
            text.fontSize = 6;
            text.color = new Color(0, 0, 0);
            textObj.transform.SetParent(instObj.transform, true);
            Vector3 textPos = new Vector3(instObj.transform.position.x, instObj.transform.position.y + 1.12f, /*instObj.transform.position.z*/RoutesInteract.MiddleLayerPosition);
            textObj.transform.position = textPos;
        }


        public void DeActivateColliders()
        {
            //InteractveElementsMainScreen.ForEach(x => { x.enabled = false;});
        }
        public void ActivateColliders()
        {
            //InteractveElementsMainScreen.ForEach(x => { x.enabled = true;});
        }
    }
