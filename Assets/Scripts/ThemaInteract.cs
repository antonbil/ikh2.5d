﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThemaInteract : MonoBehaviour {
    public List<GameObject> AngstList = new List<GameObject>();
    public List<GameObject> ConflictList = new List<GameObject>();

    public bool isAngstVisible;
    public bool isConflictVisible;

    void Awake() {
        foreach (GameObject themaObject in GameObject.FindGameObjectsWithTag("Angst")) {
            AngstList.Add(themaObject);
            themaObject.SetActive(false);
        }

        foreach (GameObject themaObject in GameObject.FindGameObjectsWithTag("Conflict")) {
            ConflictList.Add(themaObject);
            themaObject.SetActive(false);
        }
    }
    public void ThemaVisible(bool isThemaVisible, List<GameObject> themaList) {
        if (isThemaVisible) {
            foreach (GameObject themaObject in themaList) {
                themaObject.SetActive(true);
            }
        } else if (!isThemaVisible) {
            foreach (GameObject themaObject in themaList) {
                themaObject.SetActive(false);
            }
        }
    }
}