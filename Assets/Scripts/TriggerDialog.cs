﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class TriggerDialog : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
 
        // Code to execute after the delay
    }

    public void DialogStart()
    {
        print("ik ben er...");
        //ExecuteAfterTime(1);
        DecisionEntry de = new DecisionEntry(DialogInteract.DecisionDialogPrefab1, DialogInteract.DecisionButtonPrefab1, DialogInteract.GetCanvas(), "Gesprek met werkgever");
        de.AddItem("Vertel het de werkgever", x=>{DialogInteract.instanceDialogInteract.DoDialog("IKH_Dialog_script_2_S1-2", RoutesInteract.instanceRoutesInteract.BackgroundDialog);});
        de.AddItem("Stel het vertellen uit", x=>{DialogInteract.instanceDialogInteract.DoDialog("IKH_Dialog_script_1_S1-2", RoutesInteract.instanceRoutesInteract.BackgroundDialog);});
        de.Execute();
        //de.ExecuteTest();
        //DialogInteract.instanceDialogInteract.DoDialog("IKH_Dialog_script_2_S1-2", RoutesInteract.instanceRoutesInteract.BackgroundDialog);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
