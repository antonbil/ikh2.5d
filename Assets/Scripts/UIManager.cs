﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    public Camera cam;

    private bool isPinned;
    private bool isFirstTime;

    public List<GameObject> stations = new List<GameObject>(4);
    public List<GameObject> situatieCards = new List<GameObject>(4);

    private int listNumber;

    void Start() {
        //isFirstTime = false;
    }

    void Update() {
        //print(isFirstTime);
        print(isPinned);
        //print(listNumber);
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 10;

        Vector3 screenPos = cam.ScreenToWorldPoint(mousePos);

        RaycastHit2D hit = Physics2D.Raycast(screenPos, Vector2.zero);

        if (hit.collider != null) {
            //print(hit.collider.name);

            if (Input.GetMouseButtonDown(0)) {
                isPinned = !isPinned;
            }

            /*if (isFirstTime) {
                if (!scenarioCards[listNumber].gameObject.activeSelf) {
                    isPinned = false;
                } else {
                    isPinned = true;
                }
                isFirstTime = false;
            }*/

            for (int i = 0; i < stations.Count; i++) {
                if (stations[i].name == hit.collider.name) {
                    if (isFirstTime) {
                        if (!situatieCards[i].gameObject.activeSelf) {
                            isPinned = false;
                        } else {
                            isPinned = true;
                        }
                        isFirstTime = false;
                    }
                    listNumber = i;
                    //print(listNumber);

                    situatieCards[i].gameObject.SetActive(true);
                }
            }

        } else {
            isFirstTime = true;
            if (situatieCards[listNumber].gameObject.activeSelf && isPinned) {
                //print(isPinned);
                situatieCards[listNumber].gameObject.SetActive(true);
            } else {
                situatieCards[listNumber].gameObject.SetActive(false);
            }
        }
    }

    //scenarioCards[listNumber].gameObject.activeSelf
    // else if (!isPinned) {
    //     scenarioCards[listNumber].gameObject.SetActive(false);
    // }

    public void HoverandPin(Transform other) {
        for (int i = 0; i < stations.Count; i++) {
            if (stations[i].name == other.transform.name) {
                print("Wowiee");
            }
        }
    }

    /*public void HoverandPin() {
        if (Input.GetMouseButtonDown(0)) {
            isPinned = !isPinned;
        }
        for (int i = 0; i < stations.Count; i++) {
            if (stations[i].name == hit.collider.name) {
                if (isFirstTime) {
                    if (!scenarioCards[i].gameObject.activeSelf) {
                        isPinned = false;
                    } else {
                        isPinned = true;
                    }
                    isFirstTime = false;
                }
                listNumber = i;
                //print(listNumber);

                scenarioCards[i].gameObject.SetActive(true);
            }
        }

    } else {
        isFirstTime = true;
        if (scenarioCards[listNumber].gameObject.activeSelf && isPinned) {
            //print(isPinned);
            scenarioCards[listNumber].gameObject.SetActive(true);
        } else {
            scenarioCards[listNumber].gameObject.SetActive(false);
        }
    }
}*/

}