﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility {

    public static GameObject foreground;
    public static GameObject middleground;
    public static GameObject background;

    public static void SpawnLayers() {
        foreach (GameObject layerObj in Resources.LoadAll("Layers", typeof(GameObject))) {
            if (layerObj.name.Contains("fore")) {
                foreground = layerObj;
            }
            if (layerObj.name.Contains("middle")) {
                middleground = layerObj;
            }
            if (layerObj.name.Contains("back")) {
                background = layerObj;
            }
        }
    }
    public static string LoadResourceTextfile(string path)
    {
 
        string filePath = "Data/" + path.Replace(".json", "");
 
        TextAsset targetFile = Resources.Load<TextAsset>(filePath);
 
        return targetFile.text;
    }


}