rescue_svg = True
colors_role=["#ed6f82", "#a81916", "#fec700", "#75a5c6", "#2c3f92", "#08898c", "#9c9d9c"]
file_name = 'test3.svg'

num_roles_plus_1 = 8
num_areas_plus_1 = 4

def brackets(s):
    return s.replace("[[", "{").replace("]]", "}")

def write_svg(colors_role,rescue_svg,file_name, area_color = "", area_nr = 1):
    css = """
        <style type="text/css">
    /*
    to be used inside unity-css
     */
     .round-class{
     display:block;
     fill:transparent;
     stroke:#000000;
     }
     .nodezone-code1a ,.nodezone-code2a,.nodezone-code3a, .zone-background{
     stroke: transparent;
     fill:transparent;
     }
     .situation-background-within{
     stroke: transparent;
     }
    
        """
    i = 1
    for color in colors_role:
        outline_color = color
        #if color == "transparent":
        #    outline_color = "red"
        css = css + brackets("""
    .link-role-{} [[
      stroke: {}; ]]
    
    .outline-situation-link-role-{} [[
      fill: {}; ]]
        """.format(i,color,i,outline_color))
        i = i + 1
    if not rescue_svg:
        css = css + """
              .node-content, .placeholder{
      fill:transparent;}
"""
    else:
        #.edge-level1-link-role-3
        edges = []
        for i in range(1,num_roles_plus_1):
            edges.append(".edge-level1-link-role-"+str(i))

        css = css + ','.join(edges)+"{stroke:transparent;}"

    if len(area_color) > 0:
        #          .nodezone-code3aarea{
     #fill:#ffe2b4;
     #}
        css = css + brackets("""
                      .nodezone-code{}aarea[[
              fill:{};]]
        """.format(area_nr, area_color))

    css = css + """
      .arrow-head {
        stroke: lightgrey;
        stroke-width: 1;
        fill:white;
      }
      </style> 
    """

    with open('/home/anton/drive_c/tocopy/DIO/IKnowHow/OpdrachtTom/' + 'test2.svg','r') as f:
        lines = f.read()
        #lines = lines.replace("&nbsp;", " ").replace("<br>", "<br/>")
        lines = lines.replace("&nbsp;", " ").replace("<br>", "<br/>")
        # for all roles
        for i in range(1,num_roles_plus_1):
            lines = lines.replace("edge-level1 link-role-"+str(i),"edge-level1-link-role-"+str(i)+" edge-level1 link-role-"+str(i))

        if rescue_svg:
            lines = lines.replace('<svg version="1.1" width="350" height="360" class="element_svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 342.8 349.4" style="enable-background:new 0 0 342.8 349.4;" xml:space="preserve">', "<g>")\
                .replace("</svg>","</g>").strip()
            lines=lines[:-4]+"</svg>"

        header = '<svg width="100%" height="100%" xmlns:xlink="http://www.w3.org/1999/xlink" joint-selector="svg" id="v-2" xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" style="overflow: hidden; ">'
        lines = lines.replace(header,"")
        lines = header + "\n" + css + lines
        print("save to", '/home/anton/drive_c/tocopy/DIO/IKnowHow/OpdrachtTom/' + file_name)
        with open('/home/anton/drive_c/tocopy/DIO/IKnowHow/OpdrachtTom/' + file_name, 'w') as g:
            g.write(lines)

#add main file for visible shields
write_svg(colors_role,rescue_svg,file_name)

#add Routes1..7.svg
for i in range(1,num_roles_plus_1):
    my_colors = []
    for j in range(1,num_roles_plus_1):
        if i == j:
            my_colors.append(colors_role[i-1])
        else:
            my_colors.append("transparent")
    write_svg(my_colors,False,"Route{}.svg".format(i))
    
#add Area-svg's
area_colors = ["#fad1d3", "#bacde1","#ffe2b4"]
#colors for roles and edges are transparent
my_colors = ["transparent" for l in range(1,num_roles_plus_1)]
area_nr = 1
for area_color in area_colors:
    write_svg(my_colors, False, "Area{}.svg".format(area_nr), area_color, area_nr)
    area_nr = area_nr + 1


# change styles in all assets inside /home/anton/drive_c/tocopy/DIO/IKnowHow/OpdrachtTom/SVG Test IKnowHow/Assets/Assets/
# get all files in dir
from os import listdir
from os.path import isfile, join
#todo: use SVG Test IKnowHow2 instead of SVG Test IKnowHow
svg_dir = "/home/anton/drive_c/tocopy/DIO/IKnowHow/OpdrachtTom/SVG Test IKnowHow2/Assets/Assets/"
onlyfiles = [join(svg_dir, f) for f in listdir(svg_dir) if isfile(join(svg_dir, f)) and f.endswith("svg")]
print(onlyfiles)
with open('ikh.css','r') as f:
    lines = f.read()
    for svg_file in onlyfiles:
        try:
            if "COPY" in svg_file:
                continue
            with open(svg_file, 'r') as svg_file_read:
                lines_svg = svg_file_read.read()
                parts = lines_svg.split('<style>')
                last = parts[1].split('</style>')[1]
                if "IKH_GESPREK" in svg_file or "IKH_GEDACHTEN" in svg_file:
                    # make copy for every role
                    # store in "/Assets/Resources/DialogSprites/"
                    for i in range(1,num_roles_plus_1):
                        my_lines = lines + "\n.speak2{stroke:"+colors_role[i-1]+"}"+\
                        ".thought{fill:"+colors_role[i-1]+"}"
                        f_name = svg_file.replace("/IKH", "/COPY{}IKH".format(i))
                        f_name = f_name.replace("/Assets/Assets/", "/Assets/Resources/DialogSprites/")
                        total = """
        {}
        <style>
        {}
        </style>
        {}
                    """.format(parts[0],my_lines,last).strip()
                        print("save to", f_name)
                        with open(f_name, 'w') as g:
                            g.write(total)
                    continue
                total = """
{}
<style>
{}
</style>
{}
            """.format(parts[0],lines,last).strip()
                print("save to",svg_file)
                with open(svg_file, 'w') as g:
                    g.write(total)
        except:
            pass  

